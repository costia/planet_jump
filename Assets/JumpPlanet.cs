﻿using UnityEngine;
using System.Collections;

public class JumpPlanet : MonoBehaviour {
	
	public GameObject mColliding;
	GUIText mCross;
	JumpTest mTest;
	// Use this for initialization
	void Start () {
		mCross=GameObject.Find("Cross").GetComponent<GUIText>();	
		mCross.color=Color.red;
		mTest=GameObject.Find("testJumpCube").GetComponent<JumpTest>();	

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.E)){
			if ((mColliding)&&(mTest.mColliding==null)){
				Vector3 force=mColliding.transform.position-transform.parent.position;
				force.Normalize();
				PlayerControl tmp=transform.parent.gameObject.GetComponent<PlayerControl>();
				tmp.mCurrentPlanet.GetComponent<Collider>().enabled=false;
				tmp.mJumped=true;
				transform.parent.GetComponent<Rigidbody>().AddForce(force*10000);
				transform.parent.LookAt(mColliding.transform);
				transform.parent.Rotate(transform.parent.up,180);
			}
		}
	
	}
	
	void OnTriggerStay(Collider c){
		mColliding=c.gameObject;		
	}
	
	void OnTriggerExit(Collider c){
		mColliding=null;
	}
}

