﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	GameObject[] mPlanet;
	public GameObject mCurrentPlanet;
	public bool mJumped=false;
	
	// Use this for initialization
	void Start () {
		
		mPlanet=GameObject.FindGameObjectsWithTag("Planet");
	}
	
	// Update is called once per frame
	void Update () {
		float speed=1.0f;
		if (Input.GetKey(KeyCode.A)){
			this.transform.Rotate(Vector3.up,-speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.D)){
			this.transform.Rotate(Vector3.up,speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.W)){
			this.transform.Rotate(Vector3.right,-speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.S)){
			this.transform.Rotate(Vector3.right,speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.Space)){
			GetComponent<Rigidbody>().AddForce(5.0f*transform.forward);
		}
		
	}
	
	void FixedUpdate(){
		Vector3 force;
		float R;
			
		float forceMag=10.0f;
		float minR=1000000;
		int minInd=0;
		for (int i=0;i<mPlanet.Length;i++){
			force=mPlanet[i].transform.position-transform.position;
			R=Mathf.Sqrt(force.sqrMagnitude);
			if (R<minR){
				minR=R;
				minInd=i;
			}
		}
		force=mPlanet[minInd].transform.position-transform.position;		
		force.Normalize();
		force=force*forceMag;
		GetComponent<Rigidbody>().AddForce(force);	
		
		mCurrentPlanet=mPlanet[minInd];
	}
}
