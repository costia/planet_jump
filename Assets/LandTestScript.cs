﻿using UnityEngine;
using System.Collections;

public class LandTestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider c){
		PlayerControl tmp=transform.parent.gameObject.GetComponent<PlayerControl>();
		if (tmp.mJumped){
			tmp.GetComponent<Rigidbody>().velocity=Vector3.zero;
			tmp.mJumped=false;
		}
	}
}
