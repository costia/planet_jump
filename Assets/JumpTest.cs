﻿using UnityEngine;
using System.Collections;

public class JumpTest : MonoBehaviour {

	public GameObject mColliding;
	// Use this for initialization
	void Start () {
	}
	
	void OnTriggerEnter(Collider c){
		mColliding=c.gameObject;		
	}
	
	void OnTriggerExit(Collider c){
		mColliding=null;
	}
}
