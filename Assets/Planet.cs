﻿using UnityEngine;
using System.Collections;

public class Planet : MonoBehaviour {
	bool mCollisionsEnabled=true;
	float mTime;
	public Color mColor;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().material.color=mColor;
	
	}
	
	// Update is called once per frame
	void Update () {
		if ((mCollisionsEnabled)&&(GetComponent<Collider>().enabled==false)){
			mCollisionsEnabled=false;
			mTime=Time.time;
		}else if ((mCollisionsEnabled==false)&&((mTime+0.5)<Time.time)){
			mCollisionsEnabled=true;
			GetComponent<Collider>().enabled=true;
		}
	}
}
